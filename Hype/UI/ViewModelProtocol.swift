//
//  ViewModelProtocol.swift
//  Hype
//
//  Created by De Marco Johnny on 19/10/2019.
//  Copyright © 2019 De Marco Johnny. All rights reserved.
//

import Foundation

private var modelKey = "model_key"

/// Every VC needs to adhere to.
protocol ViewModelProtocol: AnyObject {
  /// placeholder for variable adhering to `ViewModel`.
  associatedtype VM: ViewModel
  
  /// The ViewModel of the `ViewController`. Once changed, the `update(oldModel: VM?)` will be called.
  /// This is initially created by the VC when the view loads
  /// Swift is inferring the Type through the `oldModel` parameter of the `update(oldModel: ViewModel?)` method
  var viewModel: VM? { get set }
  
  /// Called when the ViewModel is changed. You have to use `self.viewModel`..
  func update(oldModel: VM?)
}

/// I want to provide a default implementation for the ViewWithModel protocol.
/// In order to do so, I leverage protocol extentions
/// In order to overcome the inability of using stored values in an extention,
/// I use the associatedObject mechanism.
extension ViewModelProtocol {
  /// The ViewModel of the View. Once changed, the `update(oldModel: VM?)` will be called.
  /// The model variable is automatically created when conforming to the ViewWithModel protocol.
  /// Swift is inferring the Type through the `oldModel` parameter of the `update(oldModel: ViewModel?)` method
  var viewModel: VM? {
    get {
      return objc_getAssociatedObject(self, &modelKey) as? VM
    }
    
    set {
      let oldValue = self.viewModel
      objc_setAssociatedObject(
        self,
        &modelKey,
        newValue,
        .OBJC_ASSOCIATION_RETAIN_NONATOMIC
      )
      self.update(oldModel: oldValue)
    }
  }
}
