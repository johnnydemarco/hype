//
//  PlayerViewModel.swift
//  Hype
//
//  Created by De Marco Johnny on 20/10/2019.
//  Copyright © 2019 De Marco Johnny. All rights reserved.
//

import Foundation

struct PlayerViewModel: ViewModel {
  
  /// The player to show.
  let player: Models.Player
}
