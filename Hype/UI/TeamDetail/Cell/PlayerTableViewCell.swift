//
//  PlayerTableViewCell.swift
//  Hype
//
//  Created by De Marco Johnny on 20/10/2019.
//  Copyright © 2019 De Marco Johnny. All rights reserved.
//

import UIKit

class PlayerTableViewCell: UITableViewCell, ViewModelProtocol {
  
  // MARK: - Properties
  
  /// The name of the player.
  @IBOutlet weak var nameLabel: UILabel?
  
  /// The position of the player.
  @IBOutlet weak var positionLabel: UILabel?
  
  // MARK: - Update
  
  func update(oldModel: PlayerViewModel?) {
    self.nameLabel?.textColor = self.viewModel?.player.id == 237 ? UIColor(red: 212.0/255, green: 175.0/255, blue: 55.0/255, alpha: 1.0) : .black
    self.nameLabel?.text = "\(self.viewModel?.player.firstName ?? "") \(self.viewModel?.player.lastName ?? "")"
    self.positionLabel?.attributedText = NSAttributedString.attributedSystemFont(title: "Position: ", message: self.viewModel?.player.position?.validation(), multipleRow: false)
  }

}
