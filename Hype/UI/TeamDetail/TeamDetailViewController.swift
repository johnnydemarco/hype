//
//  TeamDetailViewController.swift
//  Hype
//
//  Created by De Marco Johnny on 19/10/2019.
//  Copyright © 2019 De Marco Johnny. All rights reserved.
//

import UIKit

class TeamDetailViewController: BaseViewController, ViewModelProtocol {
  
  // MARK: - Properties
  
  /// The table view.
  @IBOutlet weak var tableView: UITableView?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    guard let model = self.viewModel else {
      return
    }
    self.navigationItem.title = "\(model.team.fullName ?? "")".uppercased()
    self.registerNib()
  }
  
  func registerNib() {
    self.tableView?.register(
      .init(nibName: String(describing: PlayerTableViewCell.self),
            bundle: .main
      ),
      forCellReuseIdentifier: "playerID")
    self.tableView?.delegate = self
    self.tableView?.dataSource = self
  }
  
  // MARK: - Update
  
  func update(oldModel: TeamDetailViewModel?) {
    self.tableView?.reloadData()
  }
}

// MARK: - TableView

extension TeamDetailViewController: UITableViewDelegate {
  func tableView(
    _ tableView: UITableView,
    estimatedHeightForRowAt indexPath: IndexPath
  ) -> CGFloat {
    UITableView.automaticDimension
  }
  func tableView(
    _ tableView: UITableView,
    heightForRowAt indexPath: IndexPath
  ) -> CGFloat {
    UITableView.automaticDimension
  }
}

extension TeamDetailViewController: UITableViewDataSource {
  func tableView(
    _ tableView: UITableView,
    numberOfRowsInSection section: Int
  ) -> Int {
    self.viewModel?.teamPlayers.count ?? 0
  }
  
  func tableView(
    _ tableView: UITableView,
    cellForRowAt indexPath: IndexPath
  ) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "playerID") as? PlayerTableViewCell
    guard
      let playerCell = cell,
      let player = self.viewModel?.teamPlayers[indexPath.row]
      else {
        return UITableViewCell()
    }
    playerCell.selectionStyle = .none
    playerCell.viewModel = PlayerViewModel(player: player)
    return playerCell
  }
  
  func tableView(
    _ tableView: UITableView,
    didSelectRowAt indexPath: IndexPath
  ) {
    let cell = tableView.cellForRow(at: indexPath) as? PlayerTableViewCell
    let playerVC = PlayerDetailViewController(
      nibName: String(describing: PlayerDetailViewController.self),
      bundle: .main
    )
    playerVC.viewModel = cell?.viewModel
    self.navigationController?.pushViewController(playerVC, animated: true)
  }
}

