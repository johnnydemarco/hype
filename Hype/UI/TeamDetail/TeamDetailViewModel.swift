//
//  TeamDetailViewModel.swift
//  Hype
//
//  Created by De Marco Johnny on 19/10/2019.
//  Copyright © 2019 De Marco Johnny. All rights reserved.
//

import Foundation

struct TeamDetailViewModel : ViewModel{
  let team: Models.Team
  let players: [Models.Player]
  
  /// Players for the specific team.
  var teamPlayers: [Models.Player] {
    players.filter { player -> Bool in
      player.team?.id == team.id
    }.sorted {
      guard
        let position1 = $0.position,
        let position2 = $1.position
        else {
          return false
      }
      return position1 > position2
    }
  }
  
}
