//
//  TeamsViewController.swift
//  Hype
//
//  Created by De Marco Johnny on 19/10/2019.
//  Copyright © 2019 De Marco Johnny. All rights reserved.
//

import UIKit

class TeamsViewController: BaseViewController, ViewModelProtocol {
  
  // MARK: - Properties
    
  /// The tableView to represent teams.
  @IBOutlet weak var tableView: UITableView?
  
  /// The players viewmodel filled on background.
  var playersViewModel: PlayersViewModel?
  
  /// Boolean to know if players are loaded.
  var playersLoaded = false
  
  /// The completion block if someone is waiting for players api completion.
  var onPlayersWaiting: (() -> Void)?
  
  // MARK: - Setup
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.navigationItem.title = "NBA Teams"
    self.registerNib()
    self.takeTeams()
    guard let players = self.playersViewModel?.players, !players.isEmpty else {
      takePlayers()
      return
    }
  }
  
  func registerNib() {
    self.tableView?.register(
      .init(
        nibName: String(describing: TeamTableViewCell.self),
        bundle: .main),
      forCellReuseIdentifier: "teamID"
    )
    self.tableView?.delegate = self
    self.tableView?.dataSource = self
  }
  
  //MARK: - API Calls
  
  /// The request to take teams.
  func takeTeams() {
    self.showActivityIndicator()
    let request = Requests.Teams.GetAll()
    APIManager.shared.request(request: request) { result in
      self.hideActivityIndicator()
      switch result {
      case .success(let response):
        self.viewModel = TeamsViewModel(teams: response.data)
        
      case .failure(let error):
        self.showAlert(title: "Error", message: error.localizedDescription) {
          self.takeTeams()
        }
      }
    }
  }
  
  /// The request to take players on background.
  func takePlayers() {
    let request = Requests.Players.GetAll(page: self.playersViewModel?.meta.nextPage ?? 0, perPage: 100)
    APIManager.shared.request(request: request) { result in
      self.playersLoaded = false
      switch result {
      case .success(let response):
        var playersList = [Models.Player]()
        if let players = self.playersViewModel?.players {
          playersList = players
          playersList.append(contentsOf: response.data)
        }else{
          playersList = response.data
        }
        self.playersViewModel = PlayersViewModel(
          players: playersList,
          meta: response.meta
        )
        self.updatePlayers()
        
      case .failure(let error):
        self.showAlert(title: "Error", message: error.localizedDescription) {
          self.takePlayers()
          self.playersLoaded = true
        }
      }
    }
  }
  
  /// Control for players on background. There isn' t the list of players of a team so we take all the players.
  func updatePlayers(){
    guard
      let _ = self.playersViewModel?.meta.nextPage else {
        if
          let players = self.playersViewModel?.players,
          players.count > 0{
          self.playersLoaded = true
          self.onPlayersWaiting?()
        }else {
          takePlayers()
        }
        return
    }
    takePlayers()
  }
  
  // MARK: - Update
  
  func update(oldModel: TeamsViewModel?) {
    self.tableView?.reloadData()
  }
  
  // MARK: - Actions
  
  func goToDetail(teamSelected: Models.Team) {
    guard
      let players = self.playersViewModel?.players else {
        return
    }
    let detailVC = TeamDetailViewController(
      nibName: String(describing: TeamDetailViewController.self),
      bundle: .main
    )
    detailVC.viewModel = TeamDetailViewModel(team: teamSelected, players: players)
    self.navigationController?.pushViewController(detailVC, animated: true)
  }
}

// MARK: - TableView

extension TeamsViewController: UITableViewDelegate {
  func tableView(
    _ tableView: UITableView,
    estimatedHeightForRowAt indexPath: IndexPath
  ) -> CGFloat {
    120.0
  }
  func tableView(
    _ tableView: UITableView,
    heightForRowAt indexPath: IndexPath
  ) -> CGFloat {
    UITableView.automaticDimension
  }
}

extension TeamsViewController: UITableViewDataSource {
  func tableView(
    _ tableView: UITableView,
    numberOfRowsInSection section: Int
  ) -> Int {
    self.viewModel?.teams.count ?? 0
  }

  func tableView(
    _ tableView: UITableView,
    cellForRowAt indexPath: IndexPath
  ) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "teamID") as? TeamTableViewCell
    guard
      let teamCell = cell,
      let team = self.viewModel?.teams[indexPath.row]
      else {
        return UITableViewCell()
    }
    teamCell.selectionStyle = .none
    teamCell.viewModel = TeamViewModel(team: team)
    return teamCell
  }
  
  func tableView(
    _ tableView: UITableView,
    didSelectRowAt indexPath: IndexPath
  ) {
    let cell = tableView.cellForRow(at: indexPath) as? TeamTableViewCell
    if self.playersLoaded {
      guard let team = cell?.viewModel?.team else {
        return
      }
      if let nextPage = self.playersViewModel?.meta.nextPage, nextPage > 0 {
        self.takePlayers()
      }else {
        self.goToDetail(teamSelected: team)
      }
    }else{
      self.showActivityIndicator()
      self.onPlayersWaiting = {
        self.playersLoaded = true
        self.hideActivityIndicator()
        guard let team = cell?.viewModel?.team else {
          return
        }
        self.goToDetail(teamSelected: team)
      }
    }
  }
}
