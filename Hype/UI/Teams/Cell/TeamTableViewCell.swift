//
//  TeamTableViewCell.swift
//  Hype
//
//  Created by De Marco Johnny on 19/10/2019.
//  Copyright © 2019 De Marco Johnny. All rights reserved.
//

import UIKit

class TeamTableViewCell: UITableViewCell, ViewModelProtocol {
  
  // MARK: - Properties
  
  /// The abbreviation of team name.
  @IBOutlet weak var abbreviationLabel: UILabel?
  
  /// Vertical separator to have a bit of design.
  @IBOutlet weak var verticalSeparatorView: UIView?
  
  /// The full name of the team.
  @IBOutlet weak var fullNameLabel: UILabel?
  
  /// The city of the team.
  @IBOutlet weak var cityLabel: UILabel?
  
  /// The division of the team.
  @IBOutlet weak var divisionLabel: UILabel?
  
  /// The conference of the team.
  @IBOutlet weak var conferenceLabel: UILabel?
  
  // MARK: - Update
  
  func update(oldModel: TeamViewModel?) {
    self.abbreviationLabel?.text = self.viewModel?.team.abbreviation
    self.fullNameLabel?.text = self.viewModel?.team.fullName
    self.cityLabel?.attributedText = NSAttributedString.attributedSystemFont(
      title: "CITY",
      message: self.viewModel?.team.city ?? "",
      multipleRow: true
    )
    self.divisionLabel?.attributedText = NSAttributedString.attributedSystemFont(
      title: "DIV",
      message: self.viewModel?.team.division ?? "",
      multipleRow: true
    )
    self.conferenceLabel?.attributedText = NSAttributedString.attributedSystemFont(
      title: "CONF",
      message: self.viewModel?.team.conference ?? "",
      multipleRow: true
    )
  }
}
