//
//  PlayerDetailViewController.swift
//  Hype
//
//  Created by De Marco Johnny on 20/10/2019.
//  Copyright © 2019 De Marco Johnny. All rights reserved.
//

import UIKit

class PlayerDetailViewController: UIViewController , ViewModelProtocol{
  
  // MARK: - Properties
  
  /// The tableView to represent player' s info.
  @IBOutlet weak var tableView: UITableView?
  
  // MARK: - View
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.navigationItem.title = "Player Detail"
    self.registerNib()
  }
  
  func registerNib() {
    self.tableView?.register(
      .init(nibName: String(describing: PlayerDetailTableViewCell.self),
            bundle: .main
      ),
      forCellReuseIdentifier: "playerDetailID")
    self.tableView?.delegate = self
    self.tableView?.dataSource = self
  }
  
  // MARK: - Utilities
  
  /// Utility to check number and convert to String.
  /// - Parameter number: The number to check.
  func definedString(from number: Int?) -> String {
    if let num = number {
      return "\(num)"
    } else {
      return "Unknown"
    }
  }
  
  // MARK: - Update
  
  func update(oldModel: PlayerViewModel?) {}
}

// MARK: - TableView

extension PlayerDetailViewController: UITableViewDelegate {
  func tableView(
    _ tableView: UITableView,
    estimatedHeightForRowAt indexPath: IndexPath
  ) -> CGFloat {
    UITableView.automaticDimension
  }
  func tableView(
    _ tableView: UITableView,
    heightForRowAt indexPath: IndexPath
  ) -> CGFloat {
    UITableView.automaticDimension
  }
}

extension PlayerDetailViewController: UITableViewDataSource {
  func tableView(
    _ tableView: UITableView,
    numberOfRowsInSection section: Int
  ) -> Int {
    6
  }
  
  func tableView(
    _ tableView: UITableView,
    cellForRowAt indexPath: IndexPath
  ) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "playerDetailID") as? PlayerDetailTableViewCell
    guard
      let playerCell = cell,
      let player = self.viewModel?.player
      else {
        return UITableViewCell()
    }
    playerCell.selectionStyle = .none
    playerCell.isSelected = false
    switch indexPath.row {
    case 0:
      playerCell.titleLabel?.text = "Player name:"
      playerCell.valueLabel?.text = "\(player.firstName ?? "") \(player.lastName ?? "")"
      
    case 1:
      playerCell.titleLabel?.text = "Position:"
      playerCell.valueLabel?.text = player.position?.validation()
      
    case 2:
      playerCell.titleLabel?.text = "Team:"
      playerCell.valueLabel?.text = player.team?.fullName
      
    case 3:
      playerCell.titleLabel?.text = "Feet height:"
      playerCell.valueLabel?.text = definedString(from: player.heightFeet)
      
    case 4:
      playerCell.titleLabel?.text = "Inches height:"
      playerCell.valueLabel?.text = definedString(from: player.heightInches)
      
    case 5:
      playerCell.titleLabel?.text = "Pounds weight:"
      playerCell.valueLabel?.text = definedString(from: player.weightPounds)
      
    default:
      playerCell.titleLabel?.text = ""
      playerCell.valueLabel?.text = ""
      
    }
    return playerCell
  }
}

