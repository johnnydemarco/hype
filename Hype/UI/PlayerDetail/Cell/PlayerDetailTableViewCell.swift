//
//  PlayerDetailTableViewCell.swift
//  Hype
//
//  Created by De Marco Johnny on 20/10/2019.
//  Copyright © 2019 De Marco Johnny. All rights reserved.
//

import UIKit

class PlayerDetailTableViewCell: UITableViewCell {

  // MARK: - Properties
  
  /// The title of the value.
  @IBOutlet weak var titleLabel: UILabel?
  
  /// The value associated to title.
  @IBOutlet weak var valueLabel: UILabel?
}
