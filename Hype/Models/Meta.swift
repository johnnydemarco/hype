//
//  Meta.swift
//  Hype
//
//  Created by De Marco Johnny on 19/10/2019.
//  Copyright © 2019 De Marco Johnny. All rights reserved.
//

import Foundation

/// Meta model extension.
extension Models {
  /// Meta.
  struct Meta: Codable {
    enum CodingKeys: String, CodingKey {
      case totalPage
      case currentPage
      case nextPage
      case perPage
      case totalCount
    }
    let totalPage: Int?
    let currentPage: Int?
    let nextPage: Int?
    let perPage: Int?
    let totalCount: Int?
  }
}

