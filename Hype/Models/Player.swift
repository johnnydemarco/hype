//
//  Player.swift
//  Hype
//
//  Created by De Marco Johnny on 19/10/2019.
//  Copyright © 2019 De Marco Johnny. All rights reserved.
//

import Foundation

/// Player model extension.
extension Models {
  /// Player.
  struct Player: Codable {
    enum CodingKeys: String, CodingKey {
      case id
      case firstName
      case heightFeet
      case heightInches
      case lastName
      case position
      case team
      case weightPounds
    }
    let id: Int?
    let firstName: String?
    let heightFeet: Int?
    let heightInches: Int?
    let lastName: String?
    let position: String?
    let team: Team?
    let weightPounds: Int?
  }
}
