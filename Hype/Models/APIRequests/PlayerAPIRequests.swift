//
//  PlayerAPIRequests.swift
//  Hype
//
//  Created by De Marco Johnny on 19/10/2019.
//  Copyright © 2019 De Marco Johnny. All rights reserved.
//

import Foundation

extension Models.APIRequests {
  /// Namespacing for player requests.
  enum Player {}
}

extension Models.APIRequests.Player {
  /// Get the single  player.
  struct GetSinglePlayer: Encodable {
    /// The selected player id.
    let id: Int
  }
}
