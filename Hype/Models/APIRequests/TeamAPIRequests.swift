//
//  TeamAPIRequests.swift
//  Hype
//
//  Created by De Marco Johnny on 19/10/2019.
//  Copyright © 2019 De Marco Johnny. All rights reserved.
//

import Foundation

extension Models.APIRequests {
  /// Namespacing for team requests.
  enum Teams {}
}

extension Models.APIRequests.Teams {
  /// Get the single team.
  struct GetSingleTeam: Encodable {
    /// The selected team id.
    let id: Int
  }
}
