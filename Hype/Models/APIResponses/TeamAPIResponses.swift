//
//  TeamAPIResponses.swift
//  Hype
//
//  Created by De Marco Johnny on 19/10/2019.
//  Copyright © 2019 De Marco Johnny. All rights reserved.
//

import Foundation

extension Models.APIResponses {
  /// Namespacing for team responses.
  enum Teams {}
}

extension Models.APIResponses.Teams {
  /// Returns the list of all available teams..
  struct GetAll: Decodable {
    /// The list of all available teams and meta data.
    
    let data: [Models.Team]
    let meta: Models.Meta
  }
}
