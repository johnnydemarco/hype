//
//  PlayerAPIResponses.swift
//  Hype
//
//  Created by De Marco Johnny on 19/10/2019.
//  Copyright © 2019 De Marco Johnny. All rights reserved.
//

import Foundation

extension Models.APIResponses {
  /// Namespacing for player responses.
  enum Players {}
}

extension Models.APIResponses.Players {
  /// Returns the list of all players.
  struct GetAll: Decodable {
    /// The list of all players and meta data..
    let data: [Models.Player]
    let meta: Models.Meta
  }
}
