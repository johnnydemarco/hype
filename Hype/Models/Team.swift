//
//  Team.swift
//  Hype
//
//  Created by De Marco Johnny on 19/10/2019.
//  Copyright © 2019 De Marco Johnny. All rights reserved.
//

import Foundation

/// Team model extension.
extension Models {
  /// Team.
  struct Team: Codable {
    enum CodingKeys: String, CodingKey {
      case id
      case abbreviation
      case city
      case conference
      case division
      case fullName
      case name
    }
    let id: Int?
    let abbreviation: String?
    let city: String?
    let conference: String?
    let division: String?
    let fullName: String?
    let name: String?
  }
}
