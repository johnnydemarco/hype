//
//  String+Extensions.swift
//  Hype
//
//  Created by De Marco Johnny on 20/10/2019.
//  Copyright © 2019 De Marco Johnny. All rights reserved.
//

import Foundation

extension String {
  func validation() -> String {
    if self.isEmpty {
      return "N/A"
    }
    return self
  }
}
