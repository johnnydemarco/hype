//
//  UIViewController+Extensions.swift
//  Hype
//
//  Created by De Marco Johnny on 19/10/2019.
//  Copyright © 2019 De Marco Johnny. All rights reserved.
//

import Foundation
import UIKit

 
extension BaseViewController {
  
  //MARK: Show spinner
  
  /// The method to show activity on view controller.
  func showActivityIndicator() {
    self.spinner.frame = self.view.frame
    self.spinner.center = self.view.center
    self.spinner.backgroundColor = UIColor(red: 255.0/255, green: 255.0/255, blue: 255.0/255, alpha: 0.7)
    
    let loadingView: UIView = UIView()
    loadingView.frame = CGRect(x: 0,y:  0,width: 80,height: 80)
    loadingView.center = self.view.center
    loadingView.backgroundColor = UIColor(red: 68.0/255, green: 68.0/255, blue: 68.0/255, alpha: 0.7)
    loadingView.clipsToBounds = true
    loadingView.layer.cornerRadius = 10
    
    let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    actInd.frame = CGRect(x: 0, y: 0, width: 40,height: 40);
    if #available(iOS 13.0, *) {
      actInd.style = UIActivityIndicatorView.Style.large
    } else {
      actInd.style = UIActivityIndicatorView.Style.whiteLarge
    }
    actInd.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
    actInd.color = .white
    loadingView.addSubview(actInd)
    self.spinner.addSubview(loadingView)
    self.view.addSubview(self.spinner)
    actInd.startAnimating()
  }
  
  /// The method to hide activity on vie wcontroller.
  func hideActivityIndicator(){
    DispatchQueue.main.async {
      self.spinner.removeFromSuperview()
    }
  }
  
  // MARK: - Alert
  
  func showAlert(
    title: String,
    message: String,
    completion: @escaping () -> Void
  ) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
    alert.addAction(UIAlertAction(title: "Riprova", style: .default, handler: { _  in
      completion()
    }))
    self.present(alert, animated: true, completion: nil)
  }
}
