//
//  CoreTypealiases.swift
//  Hype
//
//  Created by De Marco Johnny on 19/10/2019.
//  Copyright © 2019 De Marco Johnny. All rights reserved.
//

import Alamofire
import Foundation


/// A shortcut for `Result<U, V> -> Void`
public typealias Completion<U, V: Error> = (Result<U, V>) -> Void

/// A type representing an empty response. Use `Empty.value` to get the instance.
 typealias Empty = Alamofire.Empty

/// A representation of a single HTTP header’s name / value pair.
 typealias HTTPHeaders = Alamofire.HTTPHeaders

/// HTTP method definitions.
 typealias HTTPMethod = Alamofire.HTTPMethod

/// A dictionary of parameters to apply to a `URLRequest`.
 typealias Parameters = Alamofire.Parameters

/// A type used to define how a set of parameters are applied to a URLRequest.
 typealias ParameterEncoding = Alamofire.ParameterEncoding

/// A Strings representing the path in a `URLRequest`.
 typealias Path = String

/// Creates a url-encoded query string to be set as or appended to any
/// existing URL query string or set as the HTTP body of the URL request.
/// Whether the query string is set or appended to any existing URL
/// query string or set as the HTTP body depends on the destination of the encoding.
 typealias URLEncoding = Alamofire.URLEncoding

/// Uses JSONSerialization to create a JSON representation of the parameters object,
/// which is set as the body of the request. The Content-Type HTTP header field of
/// an encoded request is set to application/json.
 typealias JSONEncoding = Alamofire.JSONEncoding
