//
//  NSAttributedString+Extensions.swift
//  Hype
//
//  Created by De Marco Johnny on 20/10/2019.
//  Copyright © 2019 De Marco Johnny. All rights reserved.
//

import Foundation
import UIKit

extension NSAttributedString {
  /// Method to create attributed String with system font on one row or multiples.
  /// - Parameter title: The semibold title.
  /// - Parameter message: the regular message.
  /// - Parameter multipleRow: title and message on differen rows.
  static func attributedSystemFont(title: String, message: String?, multipleRow: Bool) -> NSAttributedString {
    let titleText = NSMutableAttributedString(
      string: title,
      attributes: [
        .font: UIFont.systemFont(ofSize: 14, weight: .semibold)
      ]
    )
    let messageText = NSAttributedString(
      string: message ?? "",
      attributes: [
        .font: UIFont.systemFont(ofSize: 13, weight: .regular)
      ]
    )
    if multipleRow {
      titleText.append(NSAttributedString(string: "\n"))
    }
    titleText.append(messageText)
    return titleText
  }
}
