//
//  HTTPRequest.swift
//  Hype
//
//  Created by De Marco Johnny on 19/10/2019.
//  Copyright © 2019 De Marco Johnny. All rights reserved.
//

import Alamofire
import Foundation

/// A protocol that defines prerequisites to make an `HTTPRequest`.
protocol HTTPRequest: URLRequestConvertible {
  associatedtype ResponseModel: Decodable
  
  /// The HTTP method of the API call.
  var method: HTTPMethod { get }

  /// The base URL of the API call.
  var baseURL: URL { get }

  /// The path of the API call.
  var path: [Path] { get }

  /// Custom headers. Defaults to `nil`.
  var headers: HTTPHeaders? { get }

  /// Any additional query parameters to the URL. Defaults to `nil`.
  var parameters: Parameters? { get }

  /// A type used to define how a set of parameters are applied to a `URLRequest`.
  /// Defaults to `URLEncoding.default`.
  var encoding: ParameterEncoding { get }

  /// The desired Time before a timeout error is launched, in seconds.
  /// Defaults to 30s.
  var timeout: TimeInterval { get }
  
  /// The decoder to use when decoding response data.
  var dataDecoder: JSONDecoder { get }
  
  /// The encoder to use when encoding data to json.
  var dataEncoder: JSONEncoder { get }
}

// Default implementation of path, headers, parameters, encoding and timeout.
extension HTTPRequest {
  var baseURL: URL {
    URL(string: "https://free-nba.p.rapidapi.com")!
  }

  var headers: HTTPHeaders? {
    ["x-rapidapi-host": "free-nba.p.rapidapi.com",
     "x-rapidapi-key": "55a79c99a4msh743b6ce87763ef4p101d3cjsnc36673e93cd6"]
  }

  var parameters: Parameters? {
    [:]
  }

  var encoding: ParameterEncoding {
    if self.method == .post || self.method == .put {
      return JSONEncoding.default
    }
    return URLEncoding.default
  }

  var timeout: TimeInterval {
    60
  }
  
  var dataDecoder: JSONDecoder {
    DefaultSerializator.shared.dataDecoder
  }
  
  var dataEncoder: JSONEncoder {
    DefaultSerializator.shared.dataEncoder
  }
}

// Default implementation of `asURLRequest` required by `URLRequestConvertible`.
extension HTTPRequest {
  /// Serialize the `HTTPRequest` as a `URLRequest`.
  ///
  /// - Returns: a value of `URLRequest` with all the fields that are speficied in the `HTTPRequest`.
  /// - Throws: The method rethrow errors that are raised by serializers that are involved in the process.
  func asURLRequest() throws -> URLRequest {
    let url = self.path.reduce(self.baseURL) { newURL, nextPath -> URL in
      newURL.appendingPathComponent(nextPath)
    }
    var request = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: self.timeout)
    request.httpMethod = self.method.rawValue
    self.headers?.forEach { request.setValue($0.value, forHTTPHeaderField: $0.name) }
    
    return try self.encoding.encode(request, with: self.parameters)
  }
}

