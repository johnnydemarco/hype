//
//  PlayerRequests.swift
//  Hype
//
//  Created by De Marco Johnny on 19/10/2019.
//  Copyright © 2019 De Marco Johnny. All rights reserved.
//

import Foundation

extension Requests {
  /// Teams request namespace.
  enum Players {}
}

extension Requests.Players {
  struct GetAll: HTTPRequest {
    var method: HTTPMethod = .get
    var path: [Path] = ["players"]
    var parameters: Parameters?
    
    typealias ResponseModel = Models.APIResponses.Players.GetAll
    
    init(page: Int, perPage: Int){
      self.parameters = [
      "page": page,
      "per_page": perPage]
    }
  }
  
  struct GetSinglePlayer: HTTPRequest {
    var method: HTTPMethod = .get
    var path: [Path] = ["players"]

    typealias ResponseModel = Models.Player
    
    init(requestPath: Models.APIRequests.Player.GetSinglePlayer) {
      self.path.append("\(requestPath.id)")
    }
  }
}
