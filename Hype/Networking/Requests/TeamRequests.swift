//
//  TeamRequests.swift
//  Hype
//
//  Created by De Marco Johnny on 19/10/2019.
//  Copyright © 2019 De Marco Johnny. All rights reserved.
//

import Foundation

extension Requests {
  /// Teams request namespace.
  enum Teams {}
}

extension Requests.Teams {
  struct GetAll: HTTPRequest {
    var method: HTTPMethod = .get
    var path: [Path] = ["teams"]
    
    typealias ResponseModel = Models.APIResponses.Teams.GetAll
  }
  
  struct GetSingleTeam: HTTPRequest {
    var method: HTTPMethod = .get
    var path: [Path] = ["teams"]

    typealias ResponseModel = Models.Team
    
    init(requestPath: Models.APIRequests.Teams.GetSingleTeam) {
      self.path.append("\(requestPath.id)")
    }
  }
}
