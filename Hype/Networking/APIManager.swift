//
//  APIManager.swift
//  Hype
//
//  Created by De Marco Johnny on 19/10/2019.
//  Copyright © 2019 De Marco Johnny. All rights reserved.
//

import Alamofire
import Foundation

/// Possible errors from an API call.
public enum NetworkError: Error, Equatable {
  /// Generic API Error.
  case generic(error: String)
  
  /// Exceeds max waiting time.
  case timeout
  
  /// The user is not connected to the internet. AKA: NSURLErrorNotConnectedToInternet.
  case connectionOffline
}

/// A wrapper around alamofire.
class APIManager {
  
  /// A singleton.
  static let shared = APIManager()
  
  /// The session manager that is used to execute the requests.
  let session: Session
  
  /// Simple initializer. Can optionally include a custom Session instance.
  ///
  /// - Parameter session: Session instance.
  init(session: Session = .default) {
    self.session = session
  }
  
  /// Performs an HTTP request, and executes a completion handler.
  ///
  /// - Parameters:
  ///   - request: an `HTTPRequest` to execute.
  ///   - completionHandler: The completion handler to execute after the request is executed.
  func request<S: HTTPRequest>(
    request: S,
    completionHandler: @escaping (Result<S.ResponseModel, NetworkError>) -> Void
  ) {
    self.session
      .request(request)
      .validate()
      .responseDecodable(queue: .main, decoder: request.dataDecoder) { (dataResponse: DataResponse<S.ResponseModel>) in
        DispatchQueue.main.async {
          switch dataResponse.result {
          case .success(let model):
            completionHandler(.success(model))
            
          case .failure(let error):
            if (error as NSError).code == NSURLErrorTimedOut {
              completionHandler(.failure(.timeout))
              return
            }
            
            if let reachabilityManager = NetworkReachabilityManager(), !reachabilityManager.isReachable {
              completionHandler(.failure(.connectionOffline))
              return
            }
            
            completionHandler(.failure(.generic(error: error.localizedDescription)))
          }
        }
    }
  }
}
