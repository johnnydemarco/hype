//
//  DefaultSerializator.swift
//  Hype
//
//  Created by De Marco Johnny on 19/10/2019.
//  Copyright © 2019 De Marco Johnny. All rights reserved.
//

import Foundation

struct DefaultSerializator: CodingProtocol {
  
  /// A singleton.
  static let shared = DefaultSerializator()
  
  // MARK: - CodingProtocol
  
  var dataDecoder: JSONDecoder {
    let decoder = JSONDecoder()
    decoder.keyDecodingStrategy = .convertFromSnakeCase
    return decoder
  }
  
  var dataEncoder: JSONEncoder {
    let encoder = JSONEncoder()
    encoder.keyEncodingStrategy = .convertToSnakeCase
    return encoder
  }
  
  /// this struct is not meant to be initialized.
  private init() {}
}
