//
//  CodingProtocol.swift
//  Hype
//
//  Created by De Marco Johnny on 19/10/2019.
//  Copyright © 2019 De Marco Johnny. All rights reserved.
//

import Foundation

/// A protocol that dictates the rules followed when encoding and decoding JSON.
public protocol CodingProtocol {
  /// The decoder to use when decoding response data.
  var dataDecoder: JSONDecoder { get }
  
  /// The encoder to use when encoding data to json.
  var dataEncoder: JSONEncoder { get }
}
