//
//  NetworkTests.swift
//  HypeTests
//
//  Created by De Marco Johnny on 20/10/2019.
//  Copyright © 2019 De Marco Johnny. All rights reserved.
//

import XCTest
@testable import Hype

class NetworkTests: XCTestCase {

    func testGetTeamsSuccessfull() {
      struct GetTeamsRequest: HTTPRequest {
        var method: HTTPMethod = .get
        var baseURL = URL(string: "https://free-nba.p.rapidapi.com")!
        var path = ["teams"]
        typealias ResponseModel = Models.APIResponses.Teams.GetAll
      }
      
      let expectation = self.expectation(description: "request should finish")
      let apiManager = APIManager()
      let request = GetTeamsRequest()
      
      apiManager.request(request: request) { result in
        switch result {
          case .success(let response):
            XCTAssertNotNil(response.data)
            expectation.fulfill()
          
        case .failure(let error):
          XCTFail(error.localizedDescription)
          expectation.fulfill()
        }
      }
      waitForExpectations(timeout: 30, handler: nil)
    }
    
    func testGetSinglePlayer() {
      struct GetPlayerRequest: HTTPRequest {
        var method: HTTPMethod = .get
        var path: [Path] = ["players"]
        typealias ResponseModel = Models.Player
        
        init(requestPath: Models.APIRequests.Player.GetSinglePlayer) {
          self.path.append("\(requestPath.id)")
        }
      }
      
      let expectation = self.expectation(description: "request should finish")
      let apiManager = APIManager()
      let requestPath = Models.APIRequests.Player.GetSinglePlayer(id: 1)
      let request = GetPlayerRequest(requestPath: requestPath)
      
      apiManager.request(request: request) { result in
        switch result {
          case .success(let response):
            XCTAssertEqual(response.id, 1)
            expectation.fulfill()
          
        case .failure(let error):
          XCTFail(error.localizedDescription)
          expectation.fulfill()
        }
      }
      waitForExpectations(timeout: 30, handler: nil)
    }
    
    func testGetSingleTeam() {
      struct GetSingleTeam: HTTPRequest {
        var method: HTTPMethod = .get
        var path: [Path] = ["teams"]

        typealias ResponseModel = Models.Team
        
        init(requestPath: Models.APIRequests.Teams.GetSingleTeam) {
          self.path.append("\(requestPath.id)")
        }
      }
      
      let expectation = self.expectation(description: "request should finish")
      let apiManager = APIManager()
      let requestPath = Models.APIRequests.Teams.GetSingleTeam(id: 1)
      let request = GetSingleTeam(requestPath: requestPath)
      
      apiManager.request(request: request) { result in
        switch result {
          case .success(let response):
            XCTAssertEqual(response.id, 1)
            expectation.fulfill()
          
        case .failure(let error):
          XCTFail(error.localizedDescription)
          expectation.fulfill()
        }
      }
      waitForExpectations(timeout: 30, handler: nil)
    }
}
