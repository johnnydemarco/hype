//
//  StringTests.swift
//  HypeTests
//
//  Created by De Marco Johnny on 20/10/2019.
//  Copyright © 2019 De Marco Johnny. All rights reserved.
//

import XCTest
@testable import Hype

class StringTests: XCTestCase {

  func testStringEmpty() {
    let string: String = ""
    XCTAssertEqual(string.validation(), "N/A")
  }
  
  func testValidString() {
    let string: String = "pippo"
    XCTAssertNotEqual(string.validation(), "N/A")
  }

}
